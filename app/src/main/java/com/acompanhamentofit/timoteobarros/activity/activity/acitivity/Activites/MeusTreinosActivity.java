package com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.acompanhamentofit.timoteobarros.R;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Adapter.AdapterItemTreino;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Model.ItemTreino;

import java.util.ArrayList;
import java.util.List;

public class MeusTreinosActivity extends AppCompatActivity {

    private RecyclerView recyclerTreinos;
    private List<ItemTreino> treinos = new ArrayList<>();
    private AdapterItemTreino adapterItemTreino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meus_treinos);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar_treinos);
        toolbar.setTitle("Meus Treinos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerTreinos.setLayoutManager(new LinearLayoutManager(this));
        recyclerTreinos.setHasFixedSize(true);
        adapterItemTreino = new AdapterItemTreino(treinos, this);
        recyclerTreinos.setAdapter(adapterItemTreino);

        recuperarTreinos();

    }

    private void inicializarComponentes(){
        recyclerTreinos = findViewById(R.id.recyclerTreinos);
    }

    private void recuperarTreinos(){

    }

}
