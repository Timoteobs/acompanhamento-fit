package com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acompanhamentofit.timoteobarros.R;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Model.ItemTreino;

import java.util.List;

/**
 * Created by Timoteo Barros on 06/03/2019.
 */

public class AdapterItemTreino extends RecyclerView.Adapter<AdapterItemTreino.MyViewHolder> {

    private List<ItemTreino> itemTreinos;
    private Context context;

    public AdapterItemTreino(List<ItemTreino> itemTreinos, Context context) {
        this.itemTreinos = itemTreinos;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_treino, parent, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ItemTreino itemTreino = new ItemTreino();
        holder.nomeTreino.setText(itemTreino.getNomeTreino());
        holder.tipoTreino.setText(itemTreino.getTipoTreino());
        holder.descricaoTreino.setText(itemTreino.getDescricaoTreino());
    }

    @Override
    public int getItemCount() {
        return itemTreinos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView nomeTreino;
        TextView tipoTreino;
        TextView descricaoTreino;

        public MyViewHolder(View itemView){
            super(itemView);


            nomeTreino      = itemView.findViewById(R.id.textNomeTreino);
            tipoTreino      = itemView.findViewById(R.id.textTipoTreino);
            descricaoTreino = itemView.findViewById(R.id.textDescricaoTreino);


        }
    }


}
