package com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acompanhamentofit.timoteobarros.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SextaFragment extends Fragment {


    public SextaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sexta, container, false);
    }

}
