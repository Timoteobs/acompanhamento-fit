package com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Activites;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.acompanhamentofit.timoteobarros.R;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Fragments.DomingoFragment;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Fragments.QuartaFragment;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Fragments.QuintaFragment;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Fragments.SabadoFragment;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Fragments.SegundaFragment;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Fragments.SextaFragment;
import com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Fragments.TercaFragment;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

public class AcompanhamnetoActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private SmartTabLayout smartTabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acompanhamneto);

        viewPager = findViewById(R.id.view_acompanhamento);
        smartTabLayout = findViewById(R.id.viewPagerTab);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar_treinos);
        toolbar.setTitle("Acompanhamento");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //configurando abas
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(),
            FragmentPagerItems.with(this)
                    .add("Segunda-feira", SegundaFragment.class)
                    .add("Terça-feira", TercaFragment.class)
                    .add("Quarta-feira", QuartaFragment.class)
                    .add("Quinta-feira",QuintaFragment.class)
                    .add("Sexta-feira",SextaFragment.class)
                    .add("Sábado", SabadoFragment.class)
                    .add("Domingo", DomingoFragment.class)
                    .create()
        );
        viewPager.setAdapter(adapter);
        smartTabLayout.setViewPager(viewPager);



    }
}
