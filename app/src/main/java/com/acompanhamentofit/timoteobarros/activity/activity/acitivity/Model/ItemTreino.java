package com.acompanhamentofit.timoteobarros.activity.activity.acitivity.Model;

/**
 * Created by Timoteo Barros on 06/03/2019.
 */

public class ItemTreino {
    private String nomeTreino;
    private String tipoTreino;
    private String descricaoTreino;

    public ItemTreino() {
    }

    public String getNomeTreino() {
        return nomeTreino;
    }

    public void setNomeTreino(String nomeTreino) {
        this.nomeTreino = nomeTreino;
    }

    public String getTipoTreino() {
        return tipoTreino;
    }

    public void setTipoTreino(String tipoTreino) {
        this.tipoTreino = tipoTreino;
    }

    public String getDescricaoTreino() {
        return descricaoTreino;
    }

    public void setDescricaoTreino(String descricaoTreino) {
        this.descricaoTreino = descricaoTreino;
    }
}
